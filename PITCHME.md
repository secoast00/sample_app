# A React anti-pattern observed

+++

# or

+++

## PWS Air Portal 


---
## Advanced Search Form
![](AdvancedSearchForm.png)

---

### Can you see any smells? anti-patterns?

---
highlightLines: true

AdvancedSearchFormContent

```javascript
  renderRow: (row, position) ->
    `<AdvancedSearchFormRow
      key={ row.key }
      rowPosition={ position }
      term={ row }
      context={ this }
      ...
     />`

```

calls...

???

grand-parent

---

AdvancedSearchFormRow
```javascript
getAttribute: (name) ->
   this.props.context.state.formRows.getRows()[@props.rowPosition][name]

getParameterOption: ->
    parameter = @getAttribute 'parameter'
    @props.findParameterOption parameter

renderField: ->
    `<AdvancedSearchFormField
      context={ this }
      parameterOption={ this.getParameterOption() }
      />`
```

calls ...

???

parent

---
AdvancedSearchFormField
```javascript
renderTextField: ->
    `<AdvancedSearchFormTextField
      context={ this.props.context }
      parameterOption={ this.props.parameterOption }
      ...
      />`

  renderChosenField: ->
    `<AdvancedSearchFormChosenField
      context={ this.props.context }
      parameterOption={ this.props.parameterOption }
      ...
      />`

  renderDateField: ->
    `<div className='field-container'>
       <DateRangeField
       context={ this.props.context }
       ...
       ...
      />
    </div>`
```

calls ...

???

refers to grand-parent (parameterOption )
The highlighted lines are directly accessing the grand-parent

parent
---

AdvancedSearchFormTextField
```javascript
@AdvancedSearchFormTextField = React.createClass
  render: ->
    `<div className='field-container'>
      <input
        className='form-control parameter-value'
        data-parameter-value={ true }
        name={ this.props.context.getFormControlName('value') }
        onChange={ this.props.context.updateValue }
        placeholder={
          this.props.context.getPlaceholder(
            this.props.context.getAttribute('parameter')
          ) }
        type='text'
        value={ this.props.context.getAttribute('value') }
      />
    </div>`
```
???

child

---

## What sticks out?

???

Separation of concerns
tight coupling 

lets go back thru, from the top

---
highlightLines: true

AdvancedSearchFormContent

```javascript
  renderRow: (row, position) ->
    `<AdvancedSearchFormRow
      key={ row.key }
      rowPosition={ position }
      term={ row }
*     context={ this }
      ...
     />`

```

calls...

???

grand-parent

---

AdvancedSearchFormRow
```javascript
getAttribute: (name) ->
*  this.props.context.state.formRows.getRows()[@props.rowPosition][name]

getParameterOption: ->
    parameter = @getAttribute 'parameter'
    @props.findParameterOption parameter

renderField: ->
    `<AdvancedSearchFormField
*     context={ this }
      parameterOption={ this.getParameterOption() }
      />`
```

calls ...

???

parent

---
AdvancedSearchFormField
```javascript
renderTextField: ->
    `<AdvancedSearchFormTextField
*     context={ this.props.context }
*     parameterOption={ this.props.parameterOption }
      ...
      />`

  renderChosenField: ->
    `<AdvancedSearchFormChosenField
*     context={ this.props.context }
*     parameterOption={ this.props.parameterOption }
      ...
      />`

  renderDateField: ->
    `<div className='field-container'>
       <DateRangeField
*      context={ this.props.context }
       ...
       ...
      />
    </div>`
```

calls ...

???

refers to grand-parent (parameterOption )
The highlighted lines are directly accessing the grand-parent

parent
---

AdvancedSearchFormTextField
```javascript
@AdvancedSearchFormTextField = React.createClass
  render: ->
    `<div className='field-container'>
      <input
        className='form-control parameter-value'
        data-parameter-value={ true }
        name={ this.props.context.getFormControlName('value') }
*       onChange={ this.props.context.updateValue }
        placeholder={
*         this.props.context.getPlaceholder(
*           this.props.context.getAttribute('parameter')
          ) }
        type='text'
*       value={ this.props.context.getAttribute('value') }
      />
    </div>`
```
???

child

---

## Discussion

### Separation of Concerns

???

- when is it too much?
- what is the best way to spot this as a code smell


# The end? or continue if time
---

### Possible Solutions

- refactor
- Flux
- Redux
- Others?

???
- Separation of concerns
- Tight coupling

### refactor
 - remove use of context
 - reduce use of ref
 - consolidate data queries/updates to containers
 - remove business logic or at least move into containers 
 - separate additional components

### Flux
To solve this issue, much of the react community migrated to Flux a few years
back, it separates the responsibility for handling a collection of data into a
store, and the components listen to events emitted by the store to know when to
update.
 - A pattern for managing data flow
 - setup a dispatcher to handle an action, sends it to the stores, the stores
   decide what to do then emits a change event
 - See PWS Weather app for an example implementation of Flux

### Redux
More recently, many in the react community have or are migrating to Redux to
manage this sort of problem.
Its a lot like Flux except that it brings all of the application state into a
single store.
  - emit an action, reducers return the new state
  - make state mutation predictable
  - combine with RxJS for async calls
  - See RoleModel visual_therapy_ruby for an example implementation of redux
 
### Others? get input, ideas
  
### Separation of concerns

- Indicators: child components excessive access of parent (or grandparents)
  functions/data
    - use of ref
    - use of context; getting and setting a value on a parent/grandparent
    - passing all 'props', or all 'state' to a child component
    - child access to a parents state (Row getAttribute called 3 plus passed to
      child)
    - data queried from multiple places
    - business logic in the components

Use of ref:
  - ContentContainer 16, 48
  - Content 123
  - Actions 20, 54
  - Chosen Field 6,7,10,13,63

Use of context:
 - Content 36, 153, 169
 - Actions 10, 13, 19, 40, 48, 53, 72
 - Row 7, 110
 - Field 10, 16, 23, 24, 25
 - TextField 9, 10, 12, 13, 16
 - ChosenField 17, 20, 24, 56, 57

Data queried:
- ContentContainer.handleMessageFiltersSelectChange() 15
- Content.updateMessages() 87
- Content.applyFilter() 101
- Actions save button (indirect/ uses rails form) 12
- Actions apply button
- Actions save role filter button

Business logic:
- Content.getAvailableParameterOptions() 37

---

